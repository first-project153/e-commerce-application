import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ViewProduct from "./ProductList/ViewProduct.js"
import ViewCart from "./ProductList/ViewCart.js"
import ProoductList from "./ProductList/ProductList.js"




function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path="/" element={<ProoductList />} />
          <Route exact path="/viewcart" element={<ViewCart />} />
          <Route exact path="/viewproduct/:id" element={<ViewProduct />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
