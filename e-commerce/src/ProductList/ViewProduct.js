import React from "react";
import { useState } from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Products from "../product.json"
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    justifyContent: "center"
  },
  media: {
    height: 140,
  },
  maingrid: {
    marginLeft: '75px',
    marginBottom: '35px',
  },
  grid: {
    marginTop: '40px',
    width: '800px',
    marginLeft: '50px',
    marginBottom: '40px'

  },
  header: {
    backgroundColor: '#EFF5F5',
    height: '80px',
    textAlign: 'center'
  },
  btn: {
    backgroundColor: "#0066ff",
    color: "white"
  },
  img: {
    height: '300px',
    width: "400px"
  }

});

export default function ViewProduct() {

  const classes = useStyles();
  const [cart, setCart] = useState([]);

  //check the localstorage object is null or not
  const readCartFromLocalStorage = () => {
    let newCart = JSON.parse(localStorage.getItem('items'));
    if (newCart == null) {
      newCart = [];
    }
    return newCart;
  }

  //product add to cart
  const addToCart = (product) => {
    let newCart = readCartFromLocalStorage()

    let itemInCart = newCart.find(
      (item) => product.id === item.id
    );
    if (itemInCart) {
      itemInCart.quantity++;
    } else {
      itemInCart = {
        ...product,
        quantity: 1,
      }
      newCart.push(itemInCart)
    }
    setCart(newCart)
    console.log('test 2')
    localStorage.setItem('items', JSON.stringify(newCart));
  }

  const { id } = useParams();

  //get product by id
  const getProductById = (id, Products) => {
    for (var i = 0; i < Products.length; i++) {
      if (Products[i].id === id) {
        return Products[i]
      }
    }
    return null
  }
  var product = getProductById(parseInt(id), Products)

  //handle view cart page access
  const handleViewCart = () => {
    if (readCartFromLocalStorage().length <= 0) {
      alert("Cart Empty");
    } else {
      window.location = "/viewcart"
    }
  }



  return (
    <div >

      <header className={classes.header}>

        <h3>Cart Items : ({readCartFromLocalStorage().length})</h3>

        <a href="/">
          <Button style={{ backgroundColor: "#153462", color: "white" }}>Product List</Button>
        </a>


        <Button
          onClick={handleViewCart}
          style={{ backgroundColor: "#153462", color: "white", marginLeft: '15px' }}
        >View cart</Button>


      </header>


      <Card className={classes.grid} key={product.id}>

        <CardContent>
          <Typography >
            <h3>{product.productName}</h3>
          </Typography>
          <Typography>
            <img src={product.image} alt="" />
          </Typography>
          <Typography>
            <p>{product.discription}</p>
          </Typography>
          <Typography>
            <h3>Price : {product.price}</h3>
          </Typography>
        </CardContent>

        <CardActions >
          <Button style={{ backgroundColor: "#153462", color: "white" }} onClick={() => addToCart(product)} >Add To Cart</Button>
        </CardActions>

      </Card>


    </div>

  );
}