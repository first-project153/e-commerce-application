import React, { useState } from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { useNavigate } from 'react-router-dom';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    justifyContent: "center"
  },
  media: {
    height: 140,
  },
  maingrid: {
    marginLeft: '75px',
    marginBottom: '35px',
  },
  cart: {
    height: '370px',
    width: '500px',
    marginLeft: '100px',
    marginBottom: '40px'

  },
  header: {
    backgroundColor: '#EFF5F5',
    height: '80px',
    textAlign: 'center'
  },
  btn: {
    backgroundColor: "#0066ff",
    color: "white"
  },
  input: {
    width: '20px',
  }

});

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function BasicCard() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [cart, setCart] = useState([]);

  //get local storage data 
  const viewCart = () => {
    const items = JSON.parse(localStorage.getItem('items'));
    if (items !== null) {
      return items;
    }
    else {
      items = [];
      return items;
    }

  }

  //check local storage is null or not
  const readCartFromLocalStorage = () => {
    let newCart = JSON.parse(localStorage.getItem('items'));
    if (newCart == null) {
      newCart = [];
    } else
      return newCart;
  }

  //Increase the quantity
  const addItem = (product) => {

    let newCart = readCartFromLocalStorage()

    let itemInCart = newCart.find(
      (item) => product.id === item.id
    );
    if (itemInCart) {
      itemInCart.quantity++;
    } else {
      itemInCart = {
        ...product,
        quantity: 1,
      }
      newCart.push(itemInCart)
    }
    setCart(newCart)
    localStorage.setItem('items', JSON.stringify(newCart));
  }

  //reduce quantity
  const removeItem = (product) => {
    let newCart = readCartFromLocalStorage();

    let itemInCart = newCart.find(
      (item) => product.id === item.id
    );
    if (itemInCart) {
      itemInCart.quantity--;
    } else {
      itemInCart = {
        ...product,
        quantity: 0,
      }
      newCart.push(itemInCart)
    }
    setCart(newCart)
    console.log('test 2')
    localStorage.setItem('items', JSON.stringify(newCart));
  }

  //get total sum
  const getTotalSum = () => {
    return cart.reduce(
      (sum, { price, quantity }) => sum + price * quantity,
      0
    );
  };

  //get total tax
  const getTotalTax = () => {
    return cart.reduce(
      (tax, { price, quantity }) => tax + (price * quantity) * 1.23 / 100,
      0
    );
  };

  //get total amount
  const totalAmount = (price, quantity) => {
    const total = price * quantity;
    return total;
  }

  //get sub total
  const subTotal = () => {
    const total = getTotalSum() + getTotalTax() + 500;
    return total;

  }

  //remove item from cart
  const removeFromCart = (product) => {

    setCart(
      cart.filter((productToRemove) => productToRemove !== product)
    );
    console.log("cart", cart)
  };

  //clear local storage
  const clearCart = () => {
    localStorage.removeItem("items")
    alert("Cart clear successfully");
    window.location.href = "/"
  }

  return (

    <div >
      <header className={classes.header}>
        <h1 style={{ textAllign: 'center' }}>Cart Item List</h1>
        <h3 style={{ textAllign: 'center' }}>Cart Items : ({viewCart().length})</h3>

        {viewCart().length <= 0 && (
          <a href="/">
            <Button style={{ backgroundColor: "#153462", color: "white" }}>Back to Product List</Button>
          </a>
        )}


      </header>
      <Grid container direction='row' justifyContent="center">
        {viewCart().map((product) => {
          return (
            <Grid xs={8} justifyContent='center' >
              <Card className={classes.cart} key={product.id}>

                <CardContent>
                  <Typography >
                    <h3>Product Name : {product.productName}</h3>
                  </Typography>
                  <Typography >
                    <h3>Product Price : {product.price}</h3>
                  </Typography>
                  <Typography>
                    <h3>Amount : {totalAmount(product.price, product.quantity)}</h3>
                  </Typography>
                  <Typography>
                    <h3>Tax : {(product.price * product.quantity) * 1.23 / 100}</h3>
                  </Typography>
                  <Typography>
                    <h3>Quantity : {product.quantity}</h3>
                    <Button style={{ backgroundColor: "#153462", color: "white" }} onClick={() => removeItem(product)} >-</Button>
                    <Button style={{ backgroundColor: "#153462", color: "white", marginLeft: "5px" }} onClick={() => addItem(product)}>+</Button>
                  </Typography>

                  {/* <CardActions>
                    <Typography>
                      <Button style={{ backgroundColor: "#153462", color: "white" }} onClick={() => removeFromCart(product)} >Remove Item</Button>
                    </Typography>
                  </CardActions> */}

                </CardContent>
              </Card>
            </Grid>

          );
        })}
      </Grid>
      <Grid container direction='row' justifyContent="center">
        <Card style={{ width: '15%', marginRight: '60px' }}>
          <CardContent>
            <Typography>
              {viewCart().length > 0 && (
                <Button style={{ backgroundColor: "#DC3535", color: "white" }} onClick={clearCart} >Clear Cart</Button>
              )}
            </Typography>
            <br />
            <div>
              <Button onClick={handleOpen} style={{ backgroundColor: "#153462", color: "white" }}>Pay</Button>
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <Box sx={style}>
                  <Typography id="modal-modal-title" variant="h6" component="h2">
                    <h2>Summary</h2>
                  </Typography>
                  <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    <h3>Total : {getTotalSum()}</h3>
                  </Typography>
                  <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    <h3>Shipping : {500}</h3>
                  </Typography>
                  <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    <h3>Total Tax : {getTotalTax()}</h3>
                  </Typography>
                  <hr size="1" width="100%" color="black" />
                  <Typography>
                    <h3>Sub Total : {subTotal()}</h3>
                    <hr size="1" width="100%" color="black" />
                    <hr size="1" width="100%" color="black" />
                  </Typography>

                </Box>
              </Modal>
            </div>
          </CardContent>
        </Card>
      </Grid>
    </div>

  );
}