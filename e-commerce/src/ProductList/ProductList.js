import React from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Products from "../product.json"
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { useNavigate } from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        justifyContent: "center"
    },
    media: {
        height: 140,
    },
    maingrid: {
        marginLeft: '75px',
        marginBottom: '35px',
    },
    grid: {
        width: '300px',
        marginLeft: '100px',
        marginBottom: '40px'

    },
    header: {
        backgroundColor: '#EFF5F5',
        height: '80px',
        textAlign: 'center'
    },
    btn: {
        backgroundColor: "#0066ff",
        color: "white"
    },

});

export default function BasicCard() {
    const classes = useStyles();
    const navigate = useNavigate();

    //get object from local storage
    let newCart = JSON.parse(localStorage.getItem('items'));

    //check local storage null or not
    const cartItems = () => {
        if (newCart == null)
            newCart = []
        else {
            return newCart;
        }
        return newCart;
    }

    //handle view cart page
    const handleViewCart = () => {
        if (newCart.length <= 0) {
            alert("Cart Empty");
        } else {
            var link = "/viewcart";
            window.location.assign(link);
        }
    }

    return (
        <div >

            <header className={classes.header}>

                <h3>Cart Items : {cartItems().length} </h3>

                <Button
                    onClick={handleViewCart}
                    style={{ backgroundColor: "#153462", color: "white", marginLeft: '-15px' }}
                >View cart</Button>


            </header>

            <Grid container direction='row'>
                {Products.map((product) => {
                    return (
                        <Grid xs={6} justifyContent='center' >
                            <Card className={classes.grid} key={product.id}>

                                <CardContent>
                                    <Typography >
                                        <h3>{product.productName}</h3>
                                    </Typography>
                                    <Typography>
                                        <img src={product.image} alt="name" />
                                    </Typography>
                                </CardContent>

                                <CardActions >
                                    <Button style={{ backgroundColor: "#153462", color: "white" }} onClick={() => navigate('/viewproduct/' + product.id)} >View</Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    );
                })}
            </Grid>

        </div>

    );
}